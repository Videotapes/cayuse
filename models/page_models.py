from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

class BasePage:
    def __init__(self, browser):
        self.browser = browser
        self.url = 'https://www.saucedemo.com'

    def load(self):
        self.browser.get(self.url)

class SauceLoginPage(BasePage):

    USERNAME_FIELD_SEARCH = (By.ID, 'user-name')
    PASSWORD_FIELD_SEARCH = (By.ID, 'password')

    # if we were feeling particularly pedantic, we could use an __init__ method
    # with override here to append /index.html to the url attribute, but
    # this is functionally the same as what's inherited
    
    def enter_username(self, username):
        username_input_field = self.browser.find_element(*self.USERNAME_FIELD_SEARCH)
        username_input_field.send_keys(username)

    def enter_password(self, password):
        password_input_field = self.browser.find_element(*self.PASSWORD_FIELD_SEARCH)
        password_input_field.send_keys(password + Keys.RETURN)
        # an argument can be made to drop RETURN for flexibility/consistency and send it elsewhere, 
        # but for the purposes of this exercise, we'll go with it

class SauceInventoryPage(BasePage):

    def __init__(self, browser):
        super(SauceInventoryPage, self).__init__(browser)
        self.url += '/inventory.html'        

    def add_to_cart(self, purchase=None):
        if purchase:
            inventory = self.browser.find_elements_by_class_name('inventory_item')
            for item in inventory:
                if item.find_element_by_class_name('inventory_item_name').text == purchase:
                    item.find_element_by_class_name('btn_primary').click()
                    break

class SauceCartPage(BasePage):

    def __init__(self, browser):
        super(SauceCartPage, self).__init__(browser)
        self.url += '/cart.html'

    # Imagine various other page interaction methods here