# Cayuse Assessment

### Breaking the problem down
Based on the problem info, we essentially have two pieces of functionality to test: 
- user login
- cart add

### Defining test data
In this instance, our test data is already defined in the form of our 'standard_user' and associated password for testing login functionality and our 'Sauce Labs Fleece Jacket' for testing cart add functionality.  If we were to branch out beyond this narrow set of testing data, there are many directions in which we could go depending on what we are looking to test. For example, a comprehensive test data set for the golden path of cart addition would include all available items for purchase on the Sauce Labs product page (though some might consider this overkill if functionality is unchanged aside from item name). It is likely we would also want to test failure states and various other edge cases as well depending on how deep we want to go.  Our test user data, for example, may include a series of functional users and passwords, but perhaps we would test failure states by pairing known good usernames with known bad passwords or no password, etc.  Ideally, we have set the tests up such that we can simply substitute sets of data passed into them.

### Creating the tests
In approaching this assessment problem, I decided to use a combination of pytest and selenium. I created a BasePage model off of which I could extend the (technically) three additional page models for login, inventory/product and cart.  I also created two relatively basic tests to validate against successful login using 'standard_user' and successful cart addition using the 'Sauce Labs Fleece Jacket.'