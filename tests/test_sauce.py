# Go to https://www.saucedemo.com
# Log into site as standard_user
# (Technically) Validate:
    # We are logged in as standard_user/login was successful
# Add Fleece Jacket to cart
# Validate:
    # Cart contains Fleece Jacket

import pytest

from selenium.webdriver import Firefox
from models.page_models import SauceLoginPage, SauceInventoryPage, SauceCartPage


@pytest.fixture
def browser():
    # under normal circumstances, we could use a config file to assist with defining
    # attributes of our browser
    browser_driver = Firefox()
    browser_driver.implicitly_wait(10)
    yield browser_driver
    browser_driver.quit()

def test_successful_login(browser):
    login_page = SauceLoginPage(browser) 
    login_page.load()
    
    login_page.enter_username('standard_user')
    login_page.enter_password('secret_sauce')
    
    # Normally, I think it would be better to check for something more specific to login
    # but there is not any immediately obvious exposure of elements relating to a particular
    # user login outside of stored session data; the prior user session data is not reset after
    # logout either so it seems pretty inconsistent. If we really wanted to use that to validate
    # we could throw a little js at it and pull the user and verify both current session user and page load
    # ex. login_page.browser.executeScript('return sessionStorage.getItem("session-username")')
    assert login_page.browser.find_element_by_class_name('product_label')

# if login were actually required to access the inventory page,
# this test could require a dependency on the above test
def test_sauce_cart_add(browser):
    
    test_successful_login(browser) # for expediency

    inv_page = SauceInventoryPage(browser) 
    
    assert inv_page.browser.find_element_by_class_name('product_label')

    inv_page.add_to_cart('Sauce Labs Fleece Jacket')

    # we could approach validation differently here, ex. checking for the id assigned
    # to the fleece jacket elsewhere and not bothering to load into the cart page
    cart_page = SauceCartPage(browser)
    cart_page.load()

    assert cart_page.browser.find_element_by_class_name('inventory_item_name').text == 'Sauce Labs Fleece Jacket'
